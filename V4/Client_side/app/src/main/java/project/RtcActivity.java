package project;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.hardware.Camera;
import android.media.Image;
import android.media.MediaPlayer;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import org.json.JSONException;
import org.webrtc.MediaStream;
import org.webrtc.VideoRenderer;
import org.webrtc.VideoRendererGui;

import fr.pchab.androidrtc.R;
import Libraries.WebRtcClient;
import Libraries.PeerConnectionParameters;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import android.annotation.SuppressLint;

import javax.net.ssl.HttpsURLConnection;

import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Handler.Callback;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

//*********************mine***********************************************
import android.view.View;
import	java.util.Calendar;
import java.util.Iterator;

import project.com.google.zxing.integration.android.IntentIntegrator;
import project.com.google.zxing.integration.android.IntentResult;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class RtcActivity extends Activity implements WebRtcClient.RtcListener, OnClickListener{ //added  OnClickListener
    private final String FIREBASE_URL = "https://crackling-heat-6629.firebaseio.com/";
    private final static int VIDEO_CALL_SENT = 666;
    private static final String VIDEO_CODEC_VP9 = "VP9";
    private static final String AUDIO_CODEC_OPUS = "opus";
    // Local preview screen position before call is connected.
    private static final int LOCAL_X_CONNECTING = 0;
    private static final int LOCAL_Y_CONNECTING = 0;
    private static final int LOCAL_WIDTH_CONNECTING = 100;
    private static final int LOCAL_HEIGHT_CONNECTING = 100;
    // Local preview screen position after call is connected.
    private static final int LOCAL_X_CONNECTED = 72;
    private static final int LOCAL_Y_CONNECTED = 72;
    private static final int LOCAL_WIDTH_CONNECTED = 25;
    private static final int LOCAL_HEIGHT_CONNECTED = 25;
    // Remote video screen position
    private static final int REMOTE_X = 0;
    private static final int REMOTE_Y = 0;
    private static final int REMOTE_WIDTH = 100;
    private static final int REMOTE_HEIGHT = 100;
    private VideoRendererGui.ScalingType scalingType = VideoRendererGui.ScalingType.SCALE_ASPECT_FILL;
    private GLSurfaceView vsv;
    private VideoRenderer.Callbacks localRender;
    private VideoRenderer.Callbacks remoteRender;
    private WebRtcClient client;
    private String mSocketAddress;
    private String host;
    private String callerId;
    private Firebase mFirebaseRef;
    private String DATA;
    private int lock = 0;
    private String robot_id = "123456"; // instead of just ""
    private String callID = "";
    private WifiManager wifiManager;
    private MyTimerTask myTimerTask;
    private int index;
    private   byte[] buffer;
    //*****************************my code*********************************
    private final int  CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE =11;
    //*******************************************************************
    //*********************************************************************mine************************************8
    private  IntentIntegrator scanIntegrator;

    //Jonathan
    private LinkedList<Medication> medsList = new LinkedList<Medication>(); // All Meds in database
    private LinkedList<Medication> todayMeds = new LinkedList<Medication>(); //Medss till yet to be taken today
	
    ImageView imgMove;
    ImageView imgClock;
    TextView txtMed;
    MediaPlayer sound;
    ImageView imgLight;
    EditText txtSignal;
    Button btnSignal;
    // /Jonathan
    private static final int REQUEST_ENABLE_BT = 105;
    public static final int MESSAGE_READ = 106;
    //public static final String uuidStr= "00001101-0000-1000-8000-00805F9B34FB";
    public static final UUID uuidStr=UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static ArrayAdapter<String> devicesListAdapter;
    private Set<BluetoothDevice> devicesList;
    private TextView terminal;
    private EditText sendMessage;
    private final String ARDUENO_NAME="HC-06";
    private final String SDK_NAME="HC-05";
    private BluetoothAdapter mBluetoothAdapter;
    private   AlertDialog.Builder bluAlert;
    private ArrayList<DeviceItem> deviceItemList ;
    private final int ALL_NEEDED_DEVICES_BONDED=2;
    private Map<String,DevicesConnection> allDevices;
    private List<String> testList;
    //  public static UUID uuid = UUID.fromString(uuidStr);
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg)
        {  // readed message
            super.handleMessage(msg);
            switch(msg.what)
            {
                case MESSAGE_READ:
                    byte[] buffer = new byte[msg.arg1];
                    buffer = (byte[])msg.obj;
                    String s=new String(buffer);
                    break;

                default :
                    break;
            }
        }
    };
    private final Handler mHandlerSDK = new Handler() {
        @Override
        public void handleMessage(Message msg)
        {  // readed message
            super.handleMessage(msg);
            switch(msg.what)
            {
                case MESSAGE_READ:
                    byte[] bufferMsg = (byte[])msg.obj;
                    for(int i=0;i<msg.arg1;i++){
                        buffer[index]=bufferMsg[i];
                        index++;
                        if(index==99){
                            index=0;
                            updateFireBase( new String(buffer));
                        }
                    }
                    break;

                default :
                    break;
            }
        }
    };
    public static final int SCANNER_CODE = 0x0000c0de; //Only use bottom 16 bits //==REQUEST_CODE -i take it from IntentIntegrator class to check if we back from the scanner on inActvityResult
    private Button scanBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //had to do that before bluetooth init because it uses the mFirebaseRef
        //initialize firebase library
        Firebase.setAndroidContext(this);
        //initiate firebase reference
        mFirebaseRef = new Firebase(FIREBASE_URL);
        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        //------------------------------BLUETOOTH INIT----------------------------------------
        //initilaizing index for buffer use
        index=0;
        //initilaizing buffer for sdk input
        buffer= new byte[100];
        //initilaize all devices map
        allDevices=new HashMap<>();
        //initilaize the alret
        bluAlert  = new AlertDialog.Builder(this);
        bluAlert.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //dismiss the dialog
                    }
                });

        //get the BluetoothAdapter
        testList=new ArrayList<>();
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        deviceItemList = new ArrayList<DeviceItem>();
        // IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        if (null == mBluetoothAdapter) {
            mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("ErrorBT");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
                mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("ErrorCrash");
            }
            finish();
        }

        if (!mBluetoothAdapter.isEnabled()) {
            //switching on bluetooth
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        devicesList=mBluetoothAdapter.getBondedDevices();
        //CHECK IF ALL NEEDED DEVICE ARE BONDED
        if (devicesList.size() !=2/* ALL_NEEDED_DEVICES_BONDED*/){
            mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("ErrorBT");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
                mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("ErrorCache");
            }
            finish();
        }
        for (BluetoothDevice device : devicesList) {
            deviceItemList.add(new DeviceItem(device.getName(),device.getAddress()));
            try {
                if(device.getName().equals(ARDUENO_NAME))
                    allDevices.put(device.getName(), new DevicesConnection(device, new RobotBluetoothConnection(device, this, uuidStr, mHandler, MESSAGE_READ)));
                if(device.getName().equals(SDK_NAME))
                    allDevices.put(device.getName(), new DevicesConnection(device, new SDKBluetoothConnection(device, this, uuidStr, mHandlerSDK, MESSAGE_READ)));
            }catch (Exception e) {
                mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("ErrorBT");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException eI) {
                    eI.printStackTrace();
                    mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("ErrorCache");
                }
                finish();
            }
        }


        //start the bluetooth connection
        try {
            allDevices.get(ARDUENO_NAME).getbC().start();
            allDevices.get(SDK_NAME).getbC().start();
        }catch (Exception e){
            mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("ErrorBT");
        }

        //-----------------------------END BLUETOOTH INIT-------------------------------------
        wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        //this will be changed to the right one later on (automatically)
        host = "temp";
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(
                LayoutParams.FLAG_FULLSCREEN
                        | LayoutParams.FLAG_KEEP_SCREEN_ON
                        | LayoutParams.FLAG_DISMISS_KEYGUARD
                        | LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.main);

        //***************************mine********************************88
        scanBtn = (Button)findViewById(R.id.button5);
        scanBtn.setOnClickListener(this);
        scanIntegrator = new IntentIntegrator(this);
        //******************************************************************

        //this listener event is for determining 'host' value
        mFirebaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if(snapshot.child("host_ip").exists()) {
                    host = snapshot.child("host_ip").getValue().toString();
                    mFirebaseRef.child("users/try").setValue(host);
                    lock = 2;
                }
                /*for (DataSnapshot child : snapshot.getChildren()) {
                    host = snapshot.child("host_ip").getValue().toString();
                    mFirebaseRef.child("users/try").setValue(host);
                    lock = 2;
                }*/
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        mFirebaseRef.child("users/try").setValue("Temp1");
        mFirebaseRef.child("users/try").setValue("Temp2");
        mFirebaseRef.child("users/robot_"+robot_id+"/robot_response").setValue("CONNECTION_OK");
        //set the listener to handle "ISSUE_CONNECTION" and more
        mFirebaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                // test to get server_request
                /*Button b = (Button)findViewById(R.id.button);
                b.setText(snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString());*/
                if(snapshot.child("users/robot_"+robot_id+"/server_request").exists()) {
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("ISSUE_CONNECTION")) {
                        setFirebase();
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("CONNECTION_OK");
                    }
					
					//Check for movement instructions
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("GO_FORWARD")) {
                        if (null != allDevices.get(ARDUENO_NAME).getDevice() && null != allDevices.get(ARDUENO_NAME).getbC()) {
                            byte[] moveMsg = charSequenceToByteArray("mov 1");
                            //byte[] msg = charSequenceToByteArray("4");
                            allDevices.get(ARDUENO_NAME).getbC().write(moveMsg );
                        }
                        imgMove.setImageResource(R.mipmap.move_forward);
                        imgMove.setVisibility(View.VISIBLE);
                        sound = MediaPlayer.create(RtcActivity.this, R.raw.forward);
                        sound.start();
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("MOVEMENT_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("GO_LEFT")) {
                        if (null != allDevices.get(ARDUENO_NAME).getDevice() && null != allDevices.get(ARDUENO_NAME).getbC()) {
                            byte[] msg = charSequenceToByteArray("turn -90");
                            byte[] moveMsg = charSequenceToByteArray("mov 1");
                            //byte[] msg = charSequenceToByteArray("4");
                            allDevices.get(ARDUENO_NAME).getbC().write(msg);
                            allDevices.get(ARDUENO_NAME).getbC().write(moveMsg );
                        }
                        imgMove.setImageResource(R.mipmap.move_left);
                        imgMove.setVisibility(View.VISIBLE);
                        sound = MediaPlayer.create(RtcActivity.this, R.raw.left);
                        sound.start();
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("MOVEMENT_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("GO_RIGHT")) {
                        if (null != allDevices.get(ARDUENO_NAME).getDevice() && null != allDevices.get(ARDUENO_NAME).getbC()) {
                            byte[] msg = charSequenceToByteArray("turn 90");
                            byte[] moveMsg = charSequenceToByteArray("mov 1");
                            //byte[] msg = charSequenceToByteArray("4");
                            allDevices.get(ARDUENO_NAME).getbC().write(msg);
                            allDevices.get(ARDUENO_NAME).getbC().write(moveMsg );
                        }
                        imgMove.setImageResource(R.mipmap.move_right);
                        imgMove.setVisibility(View.VISIBLE);
                        sound = MediaPlayer.create(RtcActivity.this, R.raw.right);
                        sound.start();
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("MOVEMENT_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("GO_BACK")) {
                        if (null != allDevices.get(ARDUENO_NAME).getDevice() && null != allDevices.get(ARDUENO_NAME).getbC()) {
                            byte[] msg = charSequenceToByteArray("turn 180");
                            byte[] moveMsg = charSequenceToByteArray("mov 1");
                            //byte[] msg = charSequenceToByteArray("4");
                            allDevices.get(ARDUENO_NAME).getbC().write(msg);
                            allDevices.get(ARDUENO_NAME).getbC().write(moveMsg );
                        }
                        imgMove.setImageResource(R.mipmap.move_back);
                        imgMove.setVisibility(View.VISIBLE);
                        sound = MediaPlayer.create(RtcActivity.this, R.raw.back);
                        sound.start();
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("MOVEMENT_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("GO_STOP")) {
                        if (null != allDevices.get(ARDUENO_NAME).getDevice() && null != allDevices.get(ARDUENO_NAME).getbC()) {
                            byte[] msg = charSequenceToByteArray("left");
                            //byte[] msg = charSequenceToByteArray("4");
                            allDevices.get(ARDUENO_NAME).getbC().write(msg);
                        }
                        imgMove.setVisibility(View.INVISIBLE);
                        sound = MediaPlayer.create(RtcActivity.this, R.raw.stop);
                        sound.start();
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("MOVEMENT_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("RETURN_TO_BASE")) {
                        if (null != allDevices.get(ARDUENO_NAME).getDevice() && null != allDevices.get(ARDUENO_NAME).getbC()) {
                            byte[] msg = charSequenceToByteArray("park");
                            //byte[] msg = charSequenceToByteArray("4");
                            allDevices.get(ARDUENO_NAME).getbC().write(msg);
                        }
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("MOVEMENT_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("SHUT_DOWN")) {
                        Button t = (Button) findViewById(R.id.button3);
                        t.setVisibility(View.VISIBLE);
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("SHUT_DOWN_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("GET_DATA")) {
                        if(snapshot.child("users/robot_"+robot_id+"/number1").exists()&&snapshot.child("users/robot_"+robot_id+"/number2").exists()&&snapshot.child("users/robot_"+robot_id+"/number3").exists()&&snapshot.child("users/robot_"+robot_id+"/char").exists()) {
                            String n1 = snapshot.child("users/robot_"+robot_id+"/number1").getValue().toString();
                            String n2 = snapshot.child("users/robot_"+robot_id+"/number2").getValue().toString();
                            String n3 = snapshot.child("users/robot_"+robot_id+"/number3").getValue().toString();
                            String character = snapshot.child("users/robot_"+robot_id+"/char").getValue().toString();
                            int num1 = ConStringToInt(n1);
                            int num2 = ConStringToInt(n2);
                            int num3 = ConStringToInt(n3);
                            char c = character.charAt(0);
                            if (null != allDevices.get(ARDUENO_NAME).getDevice() && null != allDevices.get(ARDUENO_NAME).getbC()) {
                                byte[] msg = charSequenceToByteArray("custom cmd: "+character+", "+n1+", "+n2+", "+n3);
                                allDevices.get(ARDUENO_NAME).getbC().write(msg);
                            }
                        }
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("GET_DATA_OK");
                    }
                    //Jonathan
                    if (snapshot.child("users/robot_" + robot_id + "/Medications").exists()) {
                        //Retrieve meds from DB
                        LinkedList<Medication> updatedMedsList = new LinkedList<Medication>();
                        LinkedList<Medication> newMedsList = new LinkedList<Medication>();
                        DataSnapshot child = snapshot.child("users/robot_" + robot_id + "/Medications");
                        Iterator<String> i = ((Map<String, String>)child.getValue()).values().iterator();
                        while (i.hasNext()) {
                            String data = i.next();
                            Medication med = new Medication(data);
                            Iterator<Medication> oldMed = medsList.iterator();
                            boolean existed = false;
							
							//Check if med was already loaded before
                            while (oldMed.hasNext()){
                                Medication m = oldMed.next();
                                if(m.name.equals(med.name))
                                {
                                    existed = true;
                                    break;
                                }
                            }
                            if(!existed)
                                newMedsList.add(med);

                            updatedMedsList.add(med); //Show alert in case of new med
                        }
                        medsList = updatedMedsList;
                        ShowUpdates(newMedsList); 
                    }

                    if (snapshot.child("users/robot_" + robot_id + "/Light").exists() &&
                        snapshot.child("users/robot_" + robot_id + "/Light").getValue().toString().equals("1")) {
						//Turn on light
                        imgLight.setVisibility(View.VISIBLE);
                    }
                    else {
						//Turn off light
                        imgLight.setVisibility(View.INVISIBLE);
                    }

					if (snapshot.child("users/robot_" + robot_id + "/X-coordinates").exists() &&
                        !snapshot.child("users/robot_" + robot_id + "/X-coordinates").getValue().toString().equals("")) {
						//Get required coordinates
						
						String Xcoord = snapshot.child("users/robot_" + robot_id + "/X-coordinates").getValue().toString();
						String Ycoord = snapshot.child("users/robot_" + robot_id + "/Y-coordinates").getValue().toString();
                        sound = MediaPlayer.create(RtcActivity.this, R.raw.coord);
                        sound.start();
                        
						mFirebaseRef.child("users/robot_" + robot_id + "/X-coordinates").setValue("");
						mFirebaseRef.child("users/robot_" + robot_id + "/Y-coordinates").setValue("");

                        /*
                        AlertDialog alertDialog = new AlertDialog.Builder(RtcActivity.this).create();
                        alertDialog.setTitle("Alert");
                        alertDialog.setMessage("Going to coordinates [" + Xcoord + ", " + Ycoord + "]");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
*/
                    }

                    // /Jonathan
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        vsv = (GLSurfaceView) findViewById(R.id.glview_call);
        vsv.setPreserveEGLContextOnPause(true);
        vsv.setKeepScreenOn(true);
        VideoRendererGui.setView(vsv, new Runnable() {
            @Override
            public void run() {
                init();
            }
        });

        // local and remote render
        remoteRender = VideoRendererGui.create(
                REMOTE_X, REMOTE_Y,
                REMOTE_WIDTH, REMOTE_HEIGHT, scalingType, false);
        localRender = VideoRendererGui.create(
                LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING,
                LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING, scalingType, true);

        final Intent intent = getIntent();
        final String action = intent.getAction();

        if (Intent.ACTION_VIEW.equals(action)) {
            final List<String> segments = intent.getData().getPathSegments();
            callerId = segments.get(0);
        }
        //this is test for ConStringToInt function
        /*Button b = (Button)findViewById(R.id.button);
        String a = "NO";
        int num = ConStringToInt("2345");
        for(int i = 0; i < num; i++) {
            if (i == 2344) {
                a = "YES";
            }
            if (i < 2344) {
                a = "NO";
            }
            if (i > 2344) {
                a = "NO";
            }
        }
        b.setText(a);*/
        Timer timer = new Timer();
        myTimerTask = new MyTimerTask(mFirebaseRef,wifiManager,robot_id,medsList,todayMeds, scanIntegrator);//****************mine
        timer.schedule(myTimerTask, 0, 2000);

        //Jonathan
		
		//Load views
        imgMove = (ImageView)findViewById(R.id.imgMove);
        imgMove.setVisibility(View.INVISIBLE);
        imgClock = (ImageView)findViewById(R.id.imgClock);
        imgClock.setVisibility(View.INVISIBLE);
        txtMed = (TextView)findViewById(R.id.txtMed);
        txtMed.setVisibility(View.INVISIBLE);
        imgLight = (ImageView)findViewById(R.id.imgLight);
        imgLight.setVisibility(View.INVISIBLE);
        txtSignal = (EditText)findViewById(R.id.txtSignal);
        btnSignal = (Button)findViewById(R.id.btnSignal);
		
		//Get bluetooth transmission simulation
        btnSignal.setOnClickListener(new View.OnClickListener()   {
            public void onClick(View v)  {
                String s = txtSignal.getText().toString();
                SignalHandler(txtSignal.getText().toString());
            }
        });

		//Check once a minute if a medication alert needs to be shown
        final Handler handler = new Handler();
        Timer minuteTimer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        Calendar now = Calendar.getInstance();
                        Iterator<Medication> meds = medsList.iterator();
                        boolean existed = false;
                        while (meds.hasNext()){
                            Medication m = meds.next();
                            if(m.hour == now.get(Calendar.HOUR_OF_DAY) && m.minute == now.get(Calendar.MINUTE))
                            {
                                sound = MediaPlayer.create(RtcActivity.this, R.raw.med);
                                sound.start();
                                ShowAlert(m.name);
                                break;
                            }
                        }
                    }
                });
            }
        };
        minuteTimer.schedule(task, 0, 60000);
		
		//Update floor map in database
        /*
        btnSignal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String stub1 = "40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 8500 0 60 1 40 0 60 1 40 0 60 1 40 0 60 1 40 0 60 1 40 0   ";
                String stub2 = "40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 40 1 30 0 30 1 8500 0 70 1 30 0 70 1 30 0 70 1 30 0 70 1 30 0 70 1 30 0   ";
                mFirebaseRef.child("users/robot_" + robot_id + "/X-coordinates").setValue(stub2);
            }
        });
        */
		
        // /Jonathan
    }


    //Jonathan
	
	//Receives the list of new meds added, and shows notifications immediately
    private void ShowUpdates(LinkedList<Medication> medications)
    {
        if(medications.size() != 1)
            return;
        else
        {
            Medication m = medications.element();
            String strMed = "";
            if(m.hour < 10)
                strMed += "0";
            strMed += m.hour + ":";
            if (m.minute < 10)
                strMed += "0";
            strMed += m.minute + " - " + m.name;
            imgClock.setVisibility(View.VISIBLE);
            txtMed.setText(strMed);
            txtMed.setVisibility(View.VISIBLE);
            timerDelayRemoveClock();
        }
    }

	//Raises an alert for the patient to take a medication. To be invoked if there is a med scheduled for the current minute
    private void ShowAlert(String medName)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(RtcActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(medName);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

	//Remove med notification image after 3 seconds
    private void timerDelayRemoveClock() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                imgClock.setVisibility(View.INVISIBLE);
                txtMed.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }


	// Handle transmission received via bluetooth
    private void SignalHandler(String signal)
    {
		//Break transmission into signal and data
        int dotIndex = signal.indexOf(".");
        if(dotIndex < 0)
            dotIndex = signal.length();
        String sensor = signal.substring(0, dotIndex);
        String data = "";
        if(dotIndex < signal.length())
            data = signal.substring(dotIndex + 1, signal.length());
			
        if(sensor.equals("0")) //Fall
        {
            mFirebaseRef.child("users/robot_" + robot_id + "/Fall").setValue("1");
        }
        if(sensor.equals("1")) //Motion
        {
            Calendar now = Calendar.getInstance();
            String timeString = now.get(Calendar.HOUR_OF_DAY) + ":" + now.get(Calendar.MINUTE);
            mFirebaseRef.child("users/robot_" + robot_id + "/Motion").setValue(timeString);
        }
        if(sensor.equals("2")) //Blood pressure
        {
            Calendar now = Calendar.getInstance();
            int day = (now.DAY_OF_WEEK + 2) % 7;
            if(day == 7)
                day = 0;
            mFirebaseRef.child("users/robot_" + robot_id + "/days/day" + day).setValue(data);
        }
    }
    // /Jonathan

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override public void onReceive(Context context, Intent intent) {
            CharSequence message = "Finded device...";
            printMessageOnTerminal(message);

            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = (BluetoothDevice)intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                //ParcelUuid [] pui = (ParcelUuid [])intent.getParcelableExtra(BluetoothDevice.EXTRA_UUID)
                // Add the name and address to an array adapter to show in a ListView
                String name = device.getName();
                if(!name.isEmpty() && name.length() < 8)
                {
                    devicesListAdapter.add( name );
                    devicesList.add(device);
                }
                else
                {
                    printMessageOnTerminal("device name is empty");
                }

            }
        }
    };

    public void printMessageOnTerminal(CharSequence message) {
		/*terminal.append("\n#>");
		terminal.append(message);*/
    }

    public void smprintMessageOnTerminal(CharSequence message) {
		/*terminal.append("\n#>");
		for(int i=0; i< message.length(); i++)
		{
			int m = (int)message.charAt(i);
			terminal.append(String.valueOf(m));
			terminal.append(" ");
		}*/
    }

    public byte[] charSequenceToByteArray(CharSequence message)
    {

        int length = message.length();
        if(length == 0)
            return null;

        byte[] retval = new byte[length+1];

        for(int i=0; i<length; i++)
        {
            retval[i] = (byte) message.charAt(i);
        }
        //NEW CHANGE FOR ROBOT INTEGRATION!!
        retval[length] = (byte)'\n';
        //END OF NEW CHANGE
        return retval;
    }




    private void init() {
        Point displaySize = new Point();
        getWindowManager().getDefaultDisplay().getSize(displaySize);
        PeerConnectionParameters params = new PeerConnectionParameters(
                // 360 instead of displaySize.x and 480 instead of displaySize.y
                true, false, 360, 480, 30, 1, VIDEO_CODEC_VP9, true, 1, AUDIO_CODEC_OPUS, true);
        while(lock==0){}
        mSocketAddress = "http://" + host;
        client = new WebRtcClient(this, mSocketAddress, params, VideoRendererGui.getEGLContext());
    }

    @Override
    public void onPause() {
        super.onPause();
        vsv.onPause();
        if(client != null) {
            client.onPause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        vsv.onResume();
        if(client != null) {
            client.onResume();
        }
    }

    @Override
    public void onDestroy() {
        if(client != null) {
            client.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    public void onCallReady(String callId) {
        if (callerId != null) {
            try {
                answer(callerId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            call(callId);
        }
    }
    public void  updateFireBase(String buffer){
        int flag=0;
        String[] array=buffer.split("\r\n"),array2;
        for(int i=0;i<array.length-1;i++){
            if(array[i].equals("\r") || array[i].equals("\n") || array[i].equals("\r\n"))
                continue;
           int x= array[i].length();
            if(array[i].length()>3 || array[i].length()<2)
                continue;
            array2=array[i].split("");
            String newString="";
            int y=array2.length;
            for(int j=0;j<array2.length;j++){
                if(array2[j].equals("\r")==false && array2[j].equals("\n")==false && array2[j].equals("")==false)
                    newString=newString+array2[j];
            }
            if(array2[0].equals("0")){
                continue;
            }
            mFirebaseRef.child("users/robot_" + robot_id + "/HR").setValue(newString);
        }
    }
    public void answer(String callerId) throws JSONException {
        client.sendMessage(callerId, "init", null);
        startCam();
        Button t = (Button) findViewById(R.id.button2);
        t.setVisibility(View.VISIBLE);
    }

    public void call(String callId) {
        //setFirebase(callId); was originally here but we want the robot's id to be constant, so
        // we determine its value to be the first call id
        //here we want the robot is to be 123456 only, but i want to save the option to make it
        //more general
        if(robot_id.equals("")){
            robot_id = "123456";// and not "callId"
        }
        callID = callId;
        setFirebase();
        startCam();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VIDEO_CALL_SENT) {
            startCam();
        }
        switch(requestCode) {
            case REQUEST_ENABLE_BT:
                if(resultCode != RESULT_OK) {
                    //update firebase
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    finish();
                }
                break;

            default:
                break;
        }

        if (requestCode == SCANNER_CODE) {
            IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (scanningResult != null) {
                String scanContent = scanningResult.getContents();
                String scanFormat = scanningResult.getFormatName();
                //remove the medcation from the todayMeds list
                for (Medication med : todayMeds) {
                    if(scanContent.equals(med.getBarcode())) {
                        todayMeds.remove(med);
                        break;
                    }
                }

            }
            else{
                Toast toast = Toast.makeText(getApplicationContext(),
                        "No scan data received!", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }



    public void startCam() {
        // Camera settings
        client.start("Tele-Care Robot");
    }

    @Override
    public void onStatusChanged(final String newStatus) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), newStatus, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onLocalStream(MediaStream localStream) {
        localStream.videoTracks.get(0).addRenderer(new VideoRenderer(localRender));
        VideoRendererGui.update(localRender,
                LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING,
                LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING,
                scalingType);
    }

    @Override
    public void onAddRemoteStream(MediaStream remoteStream, int endPoint) {
        remoteStream.videoTracks.get(0).addRenderer(new VideoRenderer(remoteRender));
        VideoRendererGui.update(remoteRender,
                REMOTE_X, REMOTE_Y,
                REMOTE_WIDTH, REMOTE_HEIGHT, scalingType);
        VideoRendererGui.update(localRender,
                LOCAL_X_CONNECTED, LOCAL_Y_CONNECTED,
                LOCAL_WIDTH_CONNECTED, LOCAL_HEIGHT_CONNECTED,
                scalingType);
    }

    @Override
    public void onRemoveRemoteStream(int endPoint) {
        VideoRendererGui.update(localRender,
                LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING,
                LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING,
                scalingType);
    }

    public void setFirebase() {
        //this line was commeted because first we have to check for "ISSUE_CONNECTION" value before
        //we can answer "CONNECTION_OK"
        //mFirebaseRef.child("users/robot_"+id+"/robot_response").setValue("CONNECTION_OK");

        //the robot_id is not changing to ensure only 1 robot id until the end of the application.
        //callID is changing every time a new call is created
        mFirebaseRef.child("users/robot_"+robot_id+"/rtsp_stream_url").setValue(mSocketAddress + "/" +callID);
    }

    public void ChangeID(View view) {
        String ID = "";
        ID = ((EditText)findViewById(R.id.editText)).getText().toString();
        if (!ID.equals("")) {
            robot_id = ID;
            myTimerTask.changeID(ID);
            mFirebaseRef.child("users/robot_" + robot_id + "/bluetooth").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/char").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/hosp_ip").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/number1").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/number2").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/number3").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/rtsp_stream_url").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/signal").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/Light").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/X-coordinates").setValue("");
        }
    }

    public void Restart_Connection(View view) {
        Button t = (Button) findViewById(R.id.button3);
        t.setVisibility(View.VISIBLE);
    }

    public void CloseApp(View view) {
        System.exit(0);
    }

    public int ConStringToInt(String st){
        int num = 0;
        int n = st.length();
        for (int i = 0; i < n; i++){
            int temp = (int)st.charAt(i);
            temp-=48;
            int temp2 = (int)Math.pow(10,n-i-1);
            num+=temp*temp2;
        }
        return num;
    }

    //********************************mine******************
    @Override
    public void onClick(View v){
        if(v.getId()==R.id.button5){

            IntentIntegrator scanIntegrator = new IntentIntegrator(this);
            scanIntegrator.initiateScan();

        }
    }
}

//***************************************** BLUETOOTH CLASS***********************************
abstract  class BluetoothConnectiont extends Thread {
    //private final BluetoothServerSocket mmServerSocket;
    protected final BluetoothSocket mmServerSocket;
    protected InputStream inStream;
    protected OutputStream outStream;
    protected RtcActivity act;
    protected Handler mHandler;
    protected int MESSAGE_READ;
    protected  String a;
    //  private  UUID uuidStr;

    public  BluetoothConnectiont(BluetoothDevice device,  RtcActivity ac,UUID uuidStr,Handler mHandler,int MESSAGE_READ ) {
        BluetoothSocket tmp = null;
        inStream = null;
        outStream = null;
        act = ac;
        this.mHandler= mHandler;
        this.MESSAGE_READ=MESSAGE_READ;
        // this.uuidStr=uuidStr;

        try {
            tmp = device.createRfcommSocketToServiceRecord(uuidStr);
        } catch (IOException e) {
            CharSequence message2 = "error socket creation";
            //act.printMessageOnTerminal(message2);
        }
        mmServerSocket = tmp;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        // Get the input and output streams, using temp objects because
        // member streams are final
        try {
            tmpIn = mmServerSocket.getInputStream();
            tmpOut = mmServerSocket.getOutputStream();
        } catch (IOException e) {

        }

        inStream = tmpIn;
        outStream = tmpOut;
    }


    public abstract  void write(byte[] bytes) ;
    public byte[] charSequenceToByteArray(CharSequence message)
    {

        int length = message.length();
        if(length == 0)
            return null;

        byte[] retval = new byte[length+1];

        for(int i=0; i<length; i++)
        {
            retval[i] = (byte) message.charAt(i);
        }
        //NEW CHANGE FOR ROBOT INTEGRATION!!
        retval[length] = (byte)'\n';
        //END OF NEW CHANGE
        return retval;
    }
    /** Will cancel the listening socket, and cause the thread to finish */
    public void cancel() {
        try {
            mmServerSocket.close();
        } catch (IOException e) { }
    }
}




class RobotBluetoothConnection extends BluetoothConnectiont {
    public RobotBluetoothConnection(BluetoothDevice device,  RtcActivity ac,UUID uuidStr,Handler mHandler,int MESSAGE_READ ) {
        super(device, ac, uuidStr, mHandler, MESSAGE_READ);
    }
    public void run() {
        byte[] buffer = new byte[10];  // buffer store for the stream
        int bytes;                       // bytes returned from read()
        while (true) {
            // If a connection was accepted
            if (mmServerSocket != null) {
                if(inStream != null)
                {
                    try {

                        bytes = inStream.read(buffer);
                        mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
                                .sendToTarget();
                        a=buffer.toString();
                        byte[] msg = charSequenceToByteArray("right");
                        write(msg);

                    } catch (IOException e) {
                        break;
                    }
                }
            }

        }
    }

    public void write(byte[] bytes) {

        if(outStream != null) {
            try {
                outStream.write(bytes);
            } catch (IOException e) {
                //update firebase
            }
        }

    }


}

class SDKBluetoothConnection extends BluetoothConnectiont {


    public SDKBluetoothConnection(BluetoothDevice device,  RtcActivity ac,UUID uuidStr,Handler mHandler,int MESSAGE_READ ) {
        super(device, ac, uuidStr, mHandler, MESSAGE_READ);
    }
    public void run() {
        byte[] buffer = new byte[15];  // buffer store for the stream
        int bytes;                       // bytes returned from read()
        // Keep listening until exception occurs or a socket is returned
        try {
            //socket = mmServerSocket.accept();
            mmServerSocket.connect();
        } catch (IOException e) {
            try {
                mmServerSocket.close();
            } catch (IOException closeException) { }
            return;
        }
        while (true) {
            // If a connection was accepted
            if (mmServerSocket != null) {
                // Do work to manage the connection (in a separate thread)
                //manageConnectedSocket(socket);
                // but for now do this work in this thread
                if(inStream != null)
                {
                    try {
                        bytes = inStream.read(buffer);

                        mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
                                .sendToTarget();
                        a=buffer.toString();
                    } catch (IOException e) {
                        break;
                    }
                } else {
                }


            }

        }
    }

    public void write(byte[] bytes) {
        if(outStream != null) {
            try {
                outStream.write(bytes);
            } catch (IOException e) {
                //updateFirebase
            }
        }

    }

}
class MyTimerTask extends TimerTask {

    private Firebase mFirebaseRef;
    private WifiManager wifiManager;
    private String robot_id;
    private  LinkedList<Medication> medsList;
    private  LinkedList<Medication> todayMeds;
    private IntentIntegrator scanIntegrator;
    public MyTimerTask(Firebase FIREBASE, WifiManager WIFI, String ID,LinkedList<Medication> medsList,LinkedList<Medication> todayMeds,IntentIntegrator mscanIntegrator) {
        mFirebaseRef=FIREBASE;
        wifiManager = WIFI;
        this.medsList=medsList;
        this.todayMeds=todayMeds;
        robot_id = ID;
        scanIntegrator=mscanIntegrator;
    }

    public void changeID(String ID){
        robot_id = ID;
    }

    @Override
    public void run() {
        int numberOfLevels=5;
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int level=WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
        String st = "";
        if(level == 0){
            st = "0";
        }
        if(level == 1){
            st = "1";
        }
        if(level == 2){
            st = "2";
        }
        if(level == 3){
            st = "3";
        }
        if(level == 4){
            st = "4";
        }
        if(level == 5){
            st = "5";
        }
        mFirebaseRef.child("users/robot_" + robot_id + "/signal").setValue(st);


        //****************************************mine*******************
        Calendar rightNow = Calendar.getInstance();
        int cHour = rightNow.get(Calendar.HOUR_OF_DAY);
        int  cDay = rightNow.get(Calendar.DAY_OF_WEEK);
        int minute = rightNow.get(Calendar.MINUTE);
        //ask the patient to take his medcation now,maybe to call another run every hour
        if(minute==0){
            timeToTakeMeds( cHour);
        }

        //check if the patient didnt get his med :: after 1 hour
        checkMedTime(cHour,medsList, todayMeds);

        //reset the list for the next day
        if(cHour == 23){//we asuume that the end of the day is at 23:00

            freshDay(cDay, medsList,todayMeds);
        }





    }

    public void timeToTakeMeds(int cHour){
        for (Medication med : todayMeds) {
            if(med.getHour()==cHour){
                //update firebase
            }
        }
    }
    public void freshDay(int cDay,LinkedList<Medication> allMeds,LinkedList<Medication> todayMeds){
        todayMeds.clear();
        for (Medication med : allMeds) {
            todayMeds.add(med);
        }
    }
    public void checkMedTime(int cHour,LinkedList<Medication> allMeds,LinkedList<Medication> todayMeds){
        for (Medication med : todayMeds) {
            if(med.getHour()==cHour-1){
                //alert the doctors
                //maybe ask the patient to take his medcation agin
            }
        }
    }



}
class DeviceItem{
    private String adreass;
    private String name;
    DeviceItem(String name,String adreass){
        this.adreass=adreass;
        this.name=name;
    }

    public String getAdreass() {
        return adreass;
    }

    public String getName() {
        return name;
    }
}

class DevicesConnection{
    private BluetoothDevice device;
    private BluetoothConnectiont bC;
    DevicesConnection(BluetoothDevice device,BluetoothConnectiont bC){
        this.device=device;
        this.bC=bC;
    }

    public BluetoothConnectiont getbC() {
        return bC;
    }

    public BluetoothDevice getDevice() {
        return device;
    }
}
