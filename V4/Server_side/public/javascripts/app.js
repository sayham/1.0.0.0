var display=[];
var rc;
var favorites=[];
var localIp = '192.168.6.96:8000';
(function(){
	var app = angular.module('projectRtc', [],
		function($locationProvider){$locationProvider.html5Mode(true);}
    );
	var client = new PeerManager();
	var mediaConfig = {
        audio:true,
		video: true
    };

    app.factory('camera', ['$rootScope', '$window', function($rootScope, $window){
    	var camera = {};
    	camera.preview = $window.document.getElementById('localVideo');

    	camera.start = function(){
			return requestUserMedia(mediaConfig)
			.then(function(stream){			
				attachMediaStream(camera.preview, stream);
				client.setLocalStream(stream);
				camera.stream = stream;
				$rootScope.$broadcast('cameraIsOn',true);
			})
			.catch(Error('Failed to get access to local media.'));
		};
    	camera.stop = function(){
    		return new Promise(function(resolve, reject){			
				try {
					camera.stream.stop();
					camera.preview.src = '';
					resolve();
				} catch(error) {
					reject(error);
				}
    		})
    		.then(function(result){
    			$rootScope.$broadcast('cameraIsOn',false);
    		});	
		};
		return camera;
    }]);

	app.controller('RemoteStreamsController', ['camera', '$location', '$http', '$rootScope', function(camera, $location, $http, $rootScope){
		var rtc = this;
		rtc.remoteStreams = [];
		function getStreamById(id) {
		    for(var i=0; i<rtc.remoteStreams.length;i++) {
		    	if (rtc.remoteStreams[i].id === id) {return rtc.remoteStreams[i];}
		    }
		}
		rtc.loadData = function () {
			// get list of streams from the server
			$http.get('/streams.json').success(function(data){
				// filter own stream
				var streams = data.filter(function(stream) {
			      	return stream.id != client.getId();
			    });
			    // get former state
			    for(var i=0; i<streams.length;i++) {
			    	var stream = getStreamById(streams[i].id);
			    	streams[i].isPlaying = (!!stream) ? stream.isPLaying : false;
			    }
			    // save new streams
			    rtc.remoteStreams = streams;
			});
		};

		rtc.view = function(stream){
			client.peerInit(stream.id);
			stream.isPlaying = !stream.isPlaying;
		};
		rtc.call = function(stream){
			/* If json isn't loaded yet, construct a new stream 
			 * This happens when you load <serverUrl>/<socketId> : 
			 * it calls socketId immediatly.
			**/
			if(!stream.id){
				stream = {id: stream, isPlaying: false};
				rtc.remoteStreams.push(stream);
			}
			if(camera.isOn){
				client.toggleLocalStream(stream.id);
				if(stream.isPlaying){
					client.peerRenegociate(stream.id);
				} else {
					client.peerInit(stream.id);
				}
				stream.isPlaying = !stream.isPlaying;
			} else {
				camera.start()
				.then(function(result) {
					client.toggleLocalStream(stream.id);
					if(stream.isPlaying){
						client.peerRenegociate(stream.id);
					} else {
						client.peerInit(stream.id);
					}
					stream.isPlaying = !stream.isPlaying;
				})
				.catch(function(err) {
					console.log(err);
				});
			}
		};
		
		//initial load
		rtc.loadData();
    	if($location.url() != '/'){
			console.log("Calling location " + $location.url().slice(1));
      		rtc.call($location.url().slice(1));
    	};
		

		$rootScope.$on('startVideoCall', function(event, url) {
			console.log("Starting video call to url = " + url + " id = " + url.split("/")[3]);
      		rtc.call(url.split("/")[3]);
		});
		
	}]);

	
	var rtcStreamLink = 0;
	
	app.controller('LocalStreamController',['camera', '$scope', '$window', function(camera, $scope, $window){
		var localStream = this;
		localStream.name = 'Controller';
		localStream.link = '';
		localStream.cameraIsOn = false;

		$scope.$on('cameraIsOn', function(event,data) {
    		$scope.$apply(function() {
		    	localStream.cameraIsOn = data;
		    });
		});

		localStream.toggleCam = function(){
			if(localStream.cameraIsOn){
				camera.stop()
				.then(function(result){
					client.send('leave');
	    			client.setLocalStream(null);
				})
				.catch(function(err) {
					console.log(err);
				});
			} else {
				camera.start()
				.then(function(result) {
					//TODO - uncomment the line below and comment the line with the explicit ip when not debugging (when the server is not on local pc)
					//localStream.link = $window.location.host + '/' + client.getId();
					localStream.link = localIp + '/' + client.getId();
					rtcStreamLink = localStream.link;
					$scope.$apply();
					console('**Local stream name : ' + localStream.name);
					client.send('readyToStream', { name: localStream.name });
				})
				.catch(function(err) {
					console.log(err);
				});
			}
		};
	}]);

	app.controller('RobotController', function($rootScope, $scope, $window) {
		rc = this;
		rc.firebaseRef = {};
		rc.firebaseUsersRef = {};
		rc.users = [];
		rc.currentRobot = 0;
		rc.message_log = [];
		rc.signalStrength = 0;
		rc.fall = 0;
		rc.medications = [];
		//rc.favorites = [];
		rc.days = [];
		rc.last_seen=0;
		rc.map="";
		rc.heart_rate =0 ;
		rc.robot_place_x=0;
		rc.robot_place_y=0;
		rc.robots_stats = [];
		rc.robots_HR = [];
		rc.connectToRobot = function(robot){
			rc.currentRobot = robot;
			localStorage.setItem('current_robot',rc.currentRobot);
			console.log("connecting to robot "+ robot);
			rc.firebaseUsersRef.child(rc.currentRobot + "/robot_response").set("");
			rc.firebaseUsersRef.child(rc.currentRobot + "/rtsp_stream_url").set("");
			rc.firebaseUsersRef.child(rc.currentRobot + "/signal").set("");
			rc.firebaseUsersRef.child(rc.currentRobot + "/Fall").set("");  // *******
			rc.firebaseUsersRef.child(rc.currentRobot + "/Warning").set("");  // *******
			rc.firebaseUsersRef.child(rc.currentRobot + "/Light").set(0);  // *******
			rc.firebaseUsersRef.child(rc.currentRobot + "/Vid").set("https://www.google.co.il");  // *******
			rc.firebaseUsersRef.child(rc.currentRobot).on("child_changed", function(snapshot) {
				var field = snapshot.key();
				var robot_response = snapshot.val();
				if (field === "robot_response") {
					if (robot_response === "CONNECTION_OK") {
						rc.firebaseUsersRef.child(rc.currentRobot + "/robot_response").set("");
						rc.firebaseUsersRef.child(rc.currentRobot + "/server_request").set("");
						console.log("Successfully connected to id = " + rc.currentRobot);
					}
					if (robot_response === "MOVEMENT_OK") {
						rc.firebaseUsersRef.child(rc.currentRobot + "/robot_response").set("");
						rc.firebaseUsersRef.child(rc.currentRobot + "/server_request").set("");
						console.log("Successfully performed movement");
					}
					if (robot_response === "GET_DATA_OK") {
						rc.firebaseUsersRef.child(rc.currentRobot + "/robot_response").set("");
						console.log("Successfully received custom command");
					}
				}
				if (field === "rtsp_stream_url") {
					if (robot_response !== "") {
						console.log("Emitting event to start video call to url = " + robot_response);
						rc.firebaseUsersRef.child(rc.currentRobot + "/rtsp_stream_url").set("");
						$rootScope.$emit('startVideoCall', robot_response);
						
						//rtsp_stream_start(robot_response);
						//rc.firebaseUsersRef.child(rc.currentRobot + "/rtsp_stream_url").set("");
					}
				}
				if (field === "bluetooth") {
					rc.message_log[rc.message_log.length] = robot_response + "\n";
					console.log("Received bluetooth signal " + rc.message_log[rc.message_log.length - 1]);
					var list_to_string = "";
					for (var i in rc.message_log) {
						list_to_string += rc.message_log[rc.message_log.length - 1 - i];
					}
					$scope.robot_log = list_to_string;
					$scope.$apply();
				}
				if (field === "signal" || field === "Signal") {
					console.log("Received WiFi strength signal = " + robot_response);
					rc.signalStrength = robot_response;
					if (rc.signalStrength == 1 || rc.signalStrength == 2) {
						console.log("Bad signal, sending RETURN_TO_BASE command!");
						rc.moveRobot('RETURN_TO_BASE');
					}
					$scope.$apply();
				}
				//***************************
				if(field === "Fall"){

					if(robot_response==1) {
						console.log("im alerting!!");
						var sound1 = document.getElementById("audio2");
						sound1.play();
						alert("Fall has been Detected!");

					}
					rc.firebaseUsersRef.child(rc.currentRobot).child("Fall").set(0);
				}

				if(field === "Warning"){
					if(robot_response==1) {
						var sound2 = document.getElementById("audio3");
						sound2.play();
						alert("The Patient Did not take the medication!");

					}
					rc.firebaseUsersRef.child(rc.currentRobot).child("Warning").set(0);
				}
				if(field == "map"){
					console.log("map changed");
					rc.map=snapshot.val();
					rc.drawHouse();
				}
				if(field == "robot_place_y"){
					rc.robot_place_y=snapshot.val();
					localStorage.setItem('myX',rc.robot_place_x);
				}
				if(field == "robot_place_x"){
					rc.robot_place_x=snapshot.val();
					localStorage.setItem('myY',rc.robot_place_y);
				}
			});
			/*
				if a medication was added/changed this function will be invoked
				to update the medications list
			 */
			rc.firebaseUsersRef.child(rc.currentRobot).on("child_changed", function(snapshot){
				var field = snapshot.key();
				var medication = snapshot.val();
				if(field=="Medications") {
					rc.medications = [];
					for (var key in snapshot.val()) {
						rc.medications[rc.medications.length] = key;
					}
					$scope.medications = rc.medications;
					$scope.$apply();
				}

				if(field=="Favorites") {
					favorites = [];
					for (var key in snapshot.val()) {
						favorites[favorites.length] = key;
					}
					$scope.favorites = favorites;
					$scope.$apply();
				}

				});
			/*
			 	if a day stats was changed this function will be invoked
			 	to update the stats of blood pressure
			 */
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").on("child_changed", function(snapshot){
				var field = snapshot.key();
				console.log(field);
				if(field=="day1"){
					rc.days[0]=snapshot.val();
				}
				if(field=="day2"){
					rc.days[1]=snapshot.val();
				}
				if(field=="day3"){
					rc.days[2]=snapshot.val();
				}
				if(field=="day4"){
					rc.days[3]=snapshot.val();
				}
				if(field=="day5"){
					rc.days[4]=snapshot.val();
				}
				if(field=="day6"){
					rc.days[5]=snapshot.val();
				}
				if(field=="day7"){
					rc.days[6]=snapshot.val();
				}
				$scope.days = rc.days;
				$scope.$apply();

			});
			/*
			 	here we update the last seen status field
			 */
			rc.firebaseUsersRef.child(rc.currentRobot).on("child_changed", function(snapshot){
				var field = snapshot.key();
				console.log(field);
				if(field=="Motion"){
					rc.last_seen=snapshot.val();
				}

			});

			rc.firebaseUsersRef.child(rc.currentRobot + "/server_request").set("ISSUE_CONNECTION");
			//TODO - uncomment the line below and comment the line with the explicit ip when not debugging (when the server is not on local pc)
			//rc.firebaseUsersRef.child(rc.currentRobot + "/host_ip").set($window.location.host);
			rc.firebaseUsersRef.child(rc.currentRobot + "/host_ip").set(localIp);
			rc.databaseRetrieveMedications(rc.firebaseUsersRef.child(rc.currentRobot).child("Medications"));
			rc.databaseRetrieveFavorites(rc.firebaseUsersRef.child(rc.currentRobot).child("Favorites"));
			rc.databaseRetrieveDays();
			rc.databaseRetrieveLastSeen();
			rc.databaseRetrieveLastMap();
			rc.databaseRetrieveCoordinates();
			rc.databaseRetrieveStats();
			rc.databaseRetrieveHR();
			rc.databaseRetrieveSingleHR();
			localStorage.setItem('robots_stats', rc.robots_stats);

			console.log("storing:");
			console.log(rc.days);





		};
		
		rc.databaseRetrieveUsers = function(users_ref){
			users_ref.once("value", function(snapshot) {

				rc.users.length = 0;
				for (var key in snapshot.val()) {
					console.log(key);
					rc.users[rc.users.length] = key;
					rc.firebaseUsersRef.child(key + "/server_request").set("");
				}
				localStorage.setItem('RobotList',rc.users);
				$scope.robot_list = rc.users;
				$scope.$apply();

				
			});
		};

		rc.databaseRetrieveCoordinates =  function () {
			rc.firebaseUsersRef.child(rc.currentRobot).child("robot_place_x").once("value",function(snapshot){
			console.log("Retrieving x");
			rc.robot_place_x=snapshot.val();
			localStorage.setItem('myX',rc.robot_place_x);
		});
		rc.firebaseUsersRef.child(rc.currentRobot).child("robot_place_y").once("value",function(snapshot){
			console.log("Retrieving y");
			rc.robot_place_y=snapshot.val();
			localStorage.setItem('myY',rc.robot_place_y);
		});


		};



		rc.databaseRetrieveSingleHR = function () {
			rc.firebaseUsersRef.child(rc.currentRobot).child("HR").once("value",function(snapshot){
				rc.heart_rate=snapshot.val();
				localStorage.setItem('Single_HR',rc.heart_rate);
			});



		};

		/*
			this function loads the medication  form the fire base with every connection
		 	to the current robot. and add them to medication list witch will be shown when we decide to remove medication
		 	in the option list
		 */
		rc.databaseRetrieveMedications = function(med_ref){

			med_ref.once("value", function(snapshot) {
				rc.medications = [];
				for (var key in snapshot.val()) {
					console.log("im in once");
					rc.medications[rc.medications.length] = key;
				}

				$scope.medications = rc.medications;
				$scope.$apply();


			});

		};


		rc.databaseRetrieveFavorites = function(med_ref){
			med_ref.once("value", function(snapshot) {
				favorites = [];
				for (var key in snapshot.val()) {
					favorites[favorites.length] = key;
					console.log(key);
				}
				console.log("retrieving favorites");
				localStorage.setItem('Favs', favorites);
				$scope.favorites = favorites;
				$scope.$apply();

			});
		};


		// Getting the stats from the firebase
		rc.databaseRetrieveStats = function () {
			var robots_names = ["robot_12345" , "robot_1234567" , "robot_123456"];
			var robots_stats = [ ];
			for(var r =0 ; r < robots_names.length; r ++) {
				//var temp = [];
				console.log("at robot : " + robots_names[r]);
					var first_robot = [];
					for (var i = 1; i <= 7; i++) {
						//var url = "https://crackling-heat-6629.firebaseio.com/users/" + robots_names[r];
						//var current_robot = new Firebase(url);
						rc.firebaseUsersRef.child(robots_names[r]).child("days").child("day" + i).once("value", function (snapshot) {
							rc.robots_stats.push(snapshot.val());
							console.log("retrieving day " + i + " : " + snapshot.val());
							localStorage['robots_stats'] = rc.robots_stats;
						});
					}

					console.log("retrieving stats of robots");
					localStorage.setItem('robots_names', robots_names);
			}// for
		};


		// Getting the heart rate from the firebase/
		rc.databaseRetrieveHR = function (){
			var robots_names = ["robot_123456" , "robot_1234567" , "robot_12345"];
			var robots_HR = [ ];
			for(var r =0 ; r < robots_names.length; r ++) {

				rc.firebaseUsersRef.child(robots_names[r]).child("HR").once("value", function (snapshot) {
					rc.robots_HR.push(snapshot.val());
					console.log("retrieving HR " + r + " : " + snapshot.val());
					localStorage['robots_HR'] = rc.robots_HR;
				});


			}


		};

		rc.databaseRetrieveLastMap = function(){
			rc.firebaseUsersRef.child(rc.currentRobot).child("map").once("value",function(snapshot){
				console.log("Retrieving map");
				rc.map=snapshot.val();
				rc.drawHouse();
			});

		};



		/*
				this function retrieve the days stats form the fire base with every connection
				to the current robot.
		 */
		rc.databaseRetrieveDays = function(){
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").child("day1").once("value",function(snapshot){
				rc.days[0]=snapshot.val();
				localStorage.setItem('robot_days', rc.days);
			});
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").child("day2").once("value",function(snapshot){
				rc.days[1]=snapshot.val();
				localStorage.setItem('robot_days', rc.days);
			});
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").child("day3").once("value",function(snapshot){
				rc.days[2]=snapshot.val();
				localStorage.setItem('robot_days', rc.days);
			});
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").child("day4").once("value",function(snapshot){
				rc.days[3]=snapshot.val();
				localStorage.setItem('robot_days', rc.days);
			});
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").child("day5").once("value",function(snapshot){
				rc.days[4]=snapshot.val();
				localStorage.setItem('robot_days', rc.days);
			});
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").child("day6").once("value",function(snapshot){
				rc.days[5]=snapshot.val();
				localStorage.setItem('robot_days', rc.days);
			});
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").child("day7").once("value",function(snapshot){
				rc.days[6]=snapshot.val();
				localStorage.setItem('robot_days', rc.days);
			});


		};

		rc.databaseRetrieveLastSeen=function(){
			rc.firebaseUsersRef.child(rc.currentRobot).child("Motion").once("value",function(snapshot){
				rc.last_seen=snapshot.val();
			});

		};
		
		rc.connectToFirebase = function(addr){
			console.log("Firebase addr = " + addr);
			rc.firebaseRef = new Firebase(addr);
			
			function authHandler(error, authData) {
				if (error) {
					console.log("Login Failed!", error);
				} else {
					console.log("Authenticated successfully with payload:", authData);
				}
			}

			rc.firebaseRef.authWithPassword({
			  email    : 'tele-care@gmail.com',
			  password : '12345678'
			}, authHandler);

			//TODO - uncomment the line below and comment the line with the explicit ip when not debugging (when the server is not on local pc)
			//rc.firebaseRef.child("host_ip").set($window.location.host);
			rc.firebaseRef.child("host_ip").set(localIp);
			rc.firebaseUsersRef = rc.firebaseRef.child("users");
			rc.databaseRetrieveUsers(rc.firebaseUsersRef);
		};
		
		rc.moveRobot = function(dir){
			rc.firebaseUsersRef.child(rc.currentRobot + "/server_request").set(dir);
		};
		/*
			This function takes a medication , time and barcode
			and add them to the fire base with a new key under Medication file.
			key name : <name of the medication>
			key value: <name of the medication> <time> <barcode>
		*/
		rc.AddMedication = function(med,time,barcode){
			rc.firebaseUsersRef.child(rc.currentRobot).child("Medications").child(med).set(med+" "+time+" " +barcode);
			document.getElementById("100").value="";
			document.getElementById("time").value="";
			document.getElementById("barcode").value="";
			var sound6 = document.getElementById("audio6");
			sound6.play();

		};
		/*
		 This function takes a medication
		 and removes it from the fire base.
		 */
		rc.RemoveMedication=function(med2){
			rc.firebaseUsersRef.child(rc.currentRobot).child("Medications").child(med2).remove();
			var sound7 = document.getElementById("audio7");
			sound7.play();

		};

		/*
		 This function shows an alert
		 with the stats of the patients
		 activated when the button 'Show stats'
		 is clicked
		 */
		rc.boya=function(){
			window.alert("sunday : "+rc.days[0]+ "\n"+"monday : "+rc.days[1]
				+ "\n"+"tuesday : "+rc.days[2]+ "\n" +"wednesday : "+rc.days[3] + "\n" +
				"thursday : "+rc.days[4] +"\n" +"friday : "+rc.days[5]+"\n" +"saturday : "+rc.days[6]);

		};

		/*
		 This function shows an alert
		 with the last seen status
		 activated when the button 'Last seen is clicked'
		 is clicked
		 */
		rc.lastseen=function(){
			window.alert("last seen: " + rc.last_seen);
		};
		/*
			this function turns the lamp on and off
			when the button of the flash is clicked
			in the server.
		 */

		rc.lamp=function(){
			var x=1000;
			if(rc.currentRobot!=0) {
				rc.firebaseUsersRef.child(rc.currentRobot).child("Light").once("value",function(snapshot){
					x=snapshot.val();
					if(x==0){
						var sound4 = document.getElementById("audio4");
						sound4.play();
						rc.firebaseUsersRef.child(rc.currentRobot).child("Light").set("1");

					}else{
						var sound5 = document.getElementById("audio5");
						sound5.play();
						rc.firebaseUsersRef.child(rc.currentRobot).child("Light").set("0");
					}
				});
			}

		};
		rc.addF = function(){
			rc.firebaseUsersRef.child(rc.currentRobot).child("Fall").set("1");
		};
		/*
		*	this function opens a new page with url that in the fire base
		*	under Vid Key
		 *  */
		rc.vid = function(){
			rc.firebaseUsersRef.child(rc.currentRobot).child("Vid").once("value",function(snapshot){
				window.open(snapshot.val(),'_blank');

			});


		};



		
		rc.sendCommand = function(chr, cmd1, cmd2, cmd3) {
			console.log("Sending command = " + chr + " " + cmd1 + " " + cmd2 + " " + cmd3);
			rc.firebaseUsersRef.child(rc.currentRobot + "/char").set(chr);
			rc.firebaseUsersRef.child(rc.currentRobot + "/number1").set(cmd1);
			rc.firebaseUsersRef.child(rc.currentRobot + "/number2").set(cmd2);
			rc.firebaseUsersRef.child(rc.currentRobot + "/number3").set(cmd3);
			rc.firebaseUsersRef.child(rc.currentRobot + "/server_request").set("GET_DATA");
		};
		
		


		/*
		 This function converts the string into matrix of 1 & 0
		 Called display and stores the matrix into the local storage
			rc.map =  " x y x y x y"
			where x is the number of y's that appear in a row.
			y is binary ,  0 or 1
		 */
		rc.drawHouse = function(){
			if(rc.map == null ){
				console.log("The Map Is Empty");
			}
			var str = rc.map;
			var res = str.split(" ");
			for(var i=0 ; i< res.length ; i+=2 ){
				var symbol;
				if(Number(res[i+1])==1) symbol=1;
				else symbol=0;
				// push the symbol "x" times
				for(var j=0 ; j < Number(res[i]) ; j++){
					display.push(symbol);
				}
			}
			localStorage.setItem('home', display);

			return display;

		};
	});


})();



/*
	This function prints the Map to the screen
	its based on HTML5 canvas.
	ctx.transform(a, b, c, d, e , f);
	where :
		a	Scales the drawing horizontally.
 		b	Skew the the drawing horizontally.
 		c	Skew the the drawing vertically.
 		d	Scales the drawing vertically.
 		e	Moves the the drawing horizontally.
 		f	Moves the the drawing vertically.
 */

ShowMap = function(){
	var temp = localStorage.getItem('home');
	if(temp == null ){
		console.log("The Map Is Empty");
	}
	var myMap=[];
	var i;
	var pixels=5; // every square's size is 5x5
	var dim=100; // we are now working with 100x100 matrix
	for( i=0; i< temp.length ; i+=2){
		myMap.push(temp[i]);
	}
	var new_row=0;
	var c = document.getElementById("myCanvas");
	var ctx = c.getContext("2d");
	for( i=0 ; i < dim ;i++){
		for(var j=0 ; j < dim ; j++){
			if(new_row == 1){
				ctx.transform(1, 0, 0, 1, -1*(dim-1)*pixels , pixels);
				new_row=0;
			}
			else{
				if(j==0) ctx.transform(1, 0, 0, 1, 0,0);
				else ctx.transform(1, 0, 0, 1, pixels,0);
			}
			if(myMap[i*dim +j] == 1) ctx.fillStyle = "black";
			else ctx.fillStyle = "white";
			ctx.fillRect(0, 0, pixels, pixels);
		}
		new_row=1;
	}
	ctx.transform(1, 0, 0, 1, -1*(dim-1)*pixels , -1*(dim-1)*pixels);
	var x=localStorage.getItem('myX');
	var y=localStorage.getItem('myY');
	var img=new Image;
	img.src    = "/images/robot.jpg";
	ctx.drawImage(img, x*5, y*5);
};


/*
	This function clears the Canvas when clicking clear
 */
clearMap = function () {
	console.log("Into clearMap()");
	var canvas = document.getElementById("myCanvas");
	var ctx = canvas.getContext("2d");
	ctx.clearRect(0, 0, canvas.width, canvas.height);
};




createRobotButtons = function(){
	var rList = localStorage.getItem('RobotList');
	rList = rList.split(",");
	console.log(rList);
	console.log(rList[5]);
	for(var key in rList){
		var btn = document.createElement("BUTTON");

		document.body.appendChild(btn);
	}
};






