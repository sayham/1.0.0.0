package project;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import org.json.JSONException;
import org.webrtc.MediaStream;
import org.webrtc.VideoRenderer;
import org.webrtc.VideoRendererGui;
import fr.pchab.androidrtc.R;
import Libraries.WebRtcClient;
import Libraries.PeerConnectionParameters;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.Timer;
import java.util.TimerTask;
import android.view.View.OnClickListener;
import project.com.google.zxing.integration.android.IntentIntegrator;
import project.com.google.zxing.integration.android.IntentResult;

public class RtcActivity extends Activity implements WebRtcClient.RtcListener, OnClickListener{
    private final String FIREBASE_URL = "https://crackling-heat-6629.firebaseio.com/";
    private static final String VIDEO_CODEC_VP9 = "VP9";
    private static final String AUDIO_CODEC_OPUS = "opus";

    private int lock = 0;
    private static final int REMOTE_X = 0;
    private static final int REMOTE_Y = 0;
    private static final int REMOTE_WIDTH = 100;
    private static final int REMOTE_HEIGHT = 100;
    private final static int VIDEO_CALL_SENT = 666;
    private static final int LOCAL_X_CONNECTED = 72;
    private static final int LOCAL_Y_CONNECTED = 72;
    private static final int LOCAL_X_CONNECTING = 0;
    private static final int LOCAL_Y_CONNECTING = 0;
    private static final int LOCAL_WIDTH_CONNECTED = 25;
    private static final int LOCAL_HEIGHT_CONNECTED = 25;
    private static final int LOCAL_WIDTH_CONNECTING = 100;

    private static final int LOCAL_HEIGHT_CONNECTING = 100;
    private VideoRendererGui.ScalingType scalingType = VideoRendererGui.ScalingType.SCALE_ASPECT_FILL;

    private GLSurfaceView vsv;
    private VideoRenderer.Callbacks localRender;
    private VideoRenderer.Callbacks remoteRender;
    private WebRtcClient client;
    private String mSocketAddress;
    private String host;
    private String callerId;
    private Firebase mFirebaseRef;

    private String robot_id = "123456";
    private String callID = "";
    private WifiManager wifiManager;
    private MyTimerTask myTimerTask;

    private LinkedList<Medication> medsList = new LinkedList<Medication>();
    private LinkedList<Medication> todayMeds = new LinkedList<Medication>();

    ImageView imgMove;
    ImageView imgClock;
    TextView txtMed;
    MediaPlayer sound;
    ImageView imgLight;
    EditText txtSignal;
    Button btnSignal;

    private static final int REQUEST_ENABLE_BT = 105;
    public static final String uuidStr= "00001101-0000-1000-8000-00805F9B34FB";
    private static ArrayAdapter<String> devicesListAdapter;
    private ArrayList<BluetoothDevice> devicesList;
    private BluetoothDevice currentDevice;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothConnection bConnection;
    public static UUID uuid = UUID.fromString(uuidStr);

    public static final int SCANNER_CODE = 0x0000c0de;
    private Button scanBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        mFirebaseRef = new Firebase(FIREBASE_URL);
        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        currentDevice = null;
        bConnection = null;

        devicesList = new ArrayList<BluetoothDevice>();
        devicesListAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item);
        devicesListAdapter.add(getString(R.string.defaultSpinnerMessage));
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (null == mBluetoothAdapter) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            finish();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            } else {
                if(!getPairedDevices(devicesListAdapter)) {

                    if(!mBluetoothAdapter.startDiscovery()) {
                    } else {
                        devicesListAdapter.clear();
                    }
                }
            }
        }
        try {
            currentDevice = devicesList.get(0);
            bConnection = new BluetoothConnection(currentDevice, uuid, mFirebaseRef, robot_id);
            bConnection.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);

        host = "temp";
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(
                LayoutParams.FLAG_FULLSCREEN
                        | LayoutParams.FLAG_KEEP_SCREEN_ON
                        | LayoutParams.FLAG_DISMISS_KEYGUARD
                        | LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.main);

        scanBtn = (Button)findViewById(R.id.button5);
        scanBtn.setOnClickListener(this);

        mFirebaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if(snapshot.child("host_ip").exists()) {
                    lock = 2;
                }
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {}
        });

        mFirebaseRef.child("users/robot_"+robot_id+"/robot_response").setValue("CONNECTION_OK");
        mFirebaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if(snapshot.child("users/robot_"+robot_id+"/server_request").exists()) {
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("ISSUE_CONNECTION")) {
                        setFirebase();
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("CONNECTION_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("GO_FORWARD")) {
                        if (null != currentDevice && null != bConnection) {
                            byte[] msg = charSequenceToByteArray("forward");
                            bConnection.write(msg);
                        }
                        imgMove.setImageResource(R.mipmap.move_forward);
                        imgMove.setVisibility(View.VISIBLE);
                        sound = MediaPlayer.create(RtcActivity.this, R.raw.forward);
                        sound.start();
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("MOVEMENT_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("GO_LEFT")) {
                        if (null != currentDevice && null != bConnection) {
                            byte[] msg = charSequenceToByteArray("left");
                            bConnection.write(msg);
                        }
                        imgMove.setImageResource(R.mipmap.move_left);
                        imgMove.setVisibility(View.VISIBLE);
                        sound = MediaPlayer.create(RtcActivity.this, R.raw.left);
                        sound.start();
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("MOVEMENT_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("GO_RIGHT")) {
                        if (null != currentDevice && null != bConnection) {
                            byte[] msg = charSequenceToByteArray("right");
                            bConnection.write(msg);
                        }
                        imgMove.setImageResource(R.mipmap.move_right);
                        imgMove.setVisibility(View.VISIBLE);
                        sound = MediaPlayer.create(RtcActivity.this, R.raw.right);
                        sound.start();
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("MOVEMENT_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("GO_BACK")) {
                        if (null != currentDevice && null != bConnection) {
                            byte[] msg = charSequenceToByteArray("back");
                            //byte[] msg = charSequenceToByteArray("2");
                            bConnection.write(msg);
                        }
                        imgMove.setImageResource(R.mipmap.move_back);
                        imgMove.setVisibility(View.VISIBLE);
                        sound = MediaPlayer.create(RtcActivity.this, R.raw.back);
                        sound.start();
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("MOVEMENT_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("GO_STOP")) {
                        if (null != currentDevice && null != bConnection) {
                            byte[] msg = charSequenceToByteArray("stop");
                            bConnection.write(msg);
                        }
                        imgMove.setVisibility(View.INVISIBLE);
                        sound = MediaPlayer.create(RtcActivity.this, R.raw.stop);
                        sound.start();
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("MOVEMENT_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("RETURN_TO_BASE")) {
                        if (null != currentDevice && null != bConnection) {
                            byte[] msg = charSequenceToByteArray("return to base");
                            bConnection.write(msg);
                        }
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("MOVEMENT_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("SHUT_DOWN")) {
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("SHUT_DOWN_OK");
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/server_request").getValue().toString().equals("GET_DATA")) {
                        if(snapshot.child("users/robot_"+robot_id+"/number1").exists()&&snapshot.child("users/robot_"+robot_id+"/number2").exists()&&snapshot.child("users/robot_"+robot_id+"/number3").exists()&&snapshot.child("users/robot_"+robot_id+"/char").exists()) {
                            String n1 = snapshot.child("users/robot_"+robot_id+"/number1").getValue().toString();
                            String n2 = snapshot.child("users/robot_"+robot_id+"/number2").getValue().toString();
                            String n3 = snapshot.child("users/robot_"+robot_id+"/number3").getValue().toString();
                            String character = snapshot.child("users/robot_"+robot_id+"/char").getValue().toString();

                            if (null != currentDevice && null != bConnection) {
                                byte[] msg = charSequenceToByteArray("custom cmd: "+character+", "+n1+", "+n2+", "+n3);
                                bConnection.write(msg);
                            }
                        }
                        mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
                        mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("GET_DATA_OK");
                    }

                    if (snapshot.child("users/robot_" + robot_id + "/Medications").exists()) {
                        LinkedList<Medication> updatedMedsList = new LinkedList<Medication>();
                        LinkedList<Medication> newMedsList = new LinkedList<Medication>();
                        DataSnapshot child = snapshot.child("users/robot_" + robot_id + "/Medications");
                        Iterator<String> i = ((Map<String, String>)child.getValue()).values().iterator();
                        while (i.hasNext()) {
                            String data = i.next();
                            Medication med = new Medication(data);
                            Iterator<Medication> oldMed = medsList.iterator();
                            boolean existed = false;
                            while (oldMed.hasNext()){
                                Medication m = oldMed.next();
                                if(m.name.equals(med.name))
                                {
                                    existed = true;
                                    break;
                                }
                            }
                            if(!existed)
                                newMedsList.add(med);

                            updatedMedsList.add(med);
                        }
                        medsList = updatedMedsList;
                        ShowUpdates(newMedsList);
                    }
                    if (snapshot.child("users/robot_" + robot_id + "/Light").getValue().toString().equals("1")) {
                        imgLight.setVisibility(View.VISIBLE);
                    }
                    else {
                        imgLight.setVisibility(View.INVISIBLE);
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        vsv = (GLSurfaceView) findViewById(R.id.glview_call);
        vsv.setPreserveEGLContextOnPause(true);
        vsv.setKeepScreenOn(true);
        VideoRendererGui.setView(vsv, new Runnable() {
            @Override
            public void run() {
                init();
            }
        });

        remoteRender = VideoRendererGui.create(
                REMOTE_X, REMOTE_Y,
                REMOTE_WIDTH, REMOTE_HEIGHT, scalingType, false);
        localRender = VideoRendererGui.create(
                LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING,
                LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING, scalingType, true);

        final Intent intent = getIntent();
        final String action = intent.getAction();

        if (Intent.ACTION_VIEW.equals(action)) {
            final List<String> segments = intent.getData().getPathSegments();
            callerId = segments.get(0);
        }

        Timer timer = new Timer();
        myTimerTask = new MyTimerTask(mFirebaseRef,wifiManager,robot_id,medsList,todayMeds);
        timer.schedule(myTimerTask, 0, 2000);

        imgMove = (ImageView)findViewById(R.id.imgMove);
        imgMove.setVisibility(View.INVISIBLE);

        imgClock = (ImageView)findViewById(R.id.imgClock);
        imgClock.setVisibility(View.INVISIBLE);

        txtMed = (TextView)findViewById(R.id.txtMed);
        txtMed.setVisibility(View.INVISIBLE);

        imgLight = (ImageView)findViewById(R.id.imgLight);
        imgLight.setVisibility(View.INVISIBLE);

        txtSignal = (EditText)findViewById(R.id.txtSignal);
        btnSignal = (Button)findViewById(R.id.btnSignal);
        btnSignal.setOnClickListener(new View.OnClickListener()   {
            public void onClick(View v)  {
                SignalHandler(txtSignal.getText().toString());
            }
        });

        final Handler handler = new Handler();
        Timer minuteTimer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        Calendar now = Calendar.getInstance();
                        Iterator<Medication> meds = medsList.iterator();
                        while (meds.hasNext()){
                            Medication m = meds.next();
                            if(m.hour == now.get(Calendar.HOUR_OF_DAY) && m.minute == now.get(Calendar.MINUTE))
                            {
                                sound = MediaPlayer.create(RtcActivity.this, R.raw.med);
                                sound.start();
                                ShowAlert(m.name);
                                break;
                            }
                        }
                    }
                });
            }
        };
        minuteTimer.schedule(task, 0, 60000);
    }






    private void ShowUpdates(LinkedList<Medication> medications){
        if(medications.size() != 1)
            return;
        else
        {
            String strMed = "";
            Medication m  = medications.element();
            if(m.hour < 10)
                strMed += "0";
            strMed += m.hour + ":";

            if (m.minute < 10)
                strMed += "0";
            strMed += m.minute + " - " + m.name;

            imgClock.setVisibility(View.VISIBLE);
            txtMed.setText(strMed);
            txtMed.setVisibility(View.VISIBLE);
            timerDelayRemoveClock();
        }
    }

    private void ShowAlert(String medName){
        AlertDialog alertDialog = new AlertDialog.Builder(RtcActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(medName);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void timerDelayRemoveClock() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                imgClock.setVisibility(View.INVISIBLE);
                txtMed.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }

    private void SignalHandler(String signal) {
        String data = "";
        int dotIndex = signal.indexOf(".");

        if(dotIndex < 0) {
            dotIndex = signal.length();
        }

        String sensor = signal.substring(0, dotIndex);

        if(dotIndex < signal.length())
            data = signal.substring(dotIndex + 1, signal.length());

        if(sensor.equals("0")) {
            mFirebaseRef.child("users/robot_" + robot_id + "/Fall").setValue("1");
        }
        if(sensor.equals("1")) {
            Calendar now = Calendar.getInstance();
            String timeString = now.get(Calendar.HOUR_OF_DAY) + ":" + now.get(Calendar.MINUTE);
            mFirebaseRef.child("users/robot_" + robot_id + "/Motion").setValue(timeString);
        }

        if(sensor.equals("2")) {
            Calendar now = Calendar.getInstance();
            int day = (now.DAY_OF_WEEK + 2) % 7;
            if(day == 7)
                day = 0;
            mFirebaseRef.child("users/robot_" + robot_id + "/days/day" + day).setValue(data);
        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            printMessageOnTerminal("Finded device...");
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String name = device.getName();

                if(!name.isEmpty() && name.length() < 8) {
                    devicesListAdapter.add( name );
                    devicesList.add(device);
                }
                else {
                    printMessageOnTerminal("device name is empty");
                }
            }
        }
    };

    public void printMessageOnTerminal(CharSequence message) {
        Toast.makeText(getApplicationContext(), message.toString(), Toast.LENGTH_SHORT).show();
    }

    public byte[] charSequenceToByteArray(CharSequence message) {
        int length = message.length();
        if(length == 0)
            return null;

        byte[] retval = new byte[length+1];

        for(int i=0; i<length; i++)
        {
            retval[i] = (byte) message.charAt(i);
        }
        retval[length] = (byte)'\n';
        return retval;
    }


    private boolean getPairedDevices(ArrayAdapter<String> devices) {
        boolean retval = false;
        CharSequence message = "Searching paired devices";
        CharSequence message2 = "Search end";

        printMessageOnTerminal(message);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            retval = true;

            devices.clear();
            devices.add(getString(R.string.findedPairedDevices));

            for(BluetoothDevice device : pairedDevices) {
                devices.add(device.getName());
                devicesList.add(device);
            }
        } else {
            devices.clear();
            devices.insert(getString(R.string.noPairedDevices), 0);
        }

        printMessageOnTerminal(message2);
        return retval;
    }

    private void init() {
        Point displaySize = new Point();
        getWindowManager().getDefaultDisplay().getSize(displaySize);
        PeerConnectionParameters params = new PeerConnectionParameters(
                true, false, 360, 480, 30, 1, VIDEO_CODEC_VP9, true, 1, AUDIO_CODEC_OPUS, true);
        while(lock==0){}
        mSocketAddress = "http://" + host;
        client = new WebRtcClient(this, mSocketAddress, params, VideoRendererGui.getEGLContext());
    }

    @Override
    public void onPause() {
        super.onPause();
        vsv.onPause();
        if(client != null) {
            client.onPause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        vsv.onResume();
        if(client != null) {
            client.onResume();
        }
    }

    @Override
    public void onDestroy() {
        if(client != null) {
            client.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    public void onCallReady(String callId) {
        if (callerId != null) {
            try {
                client.sendMessage(callerId, "init", null);
                startCam();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            call(callId);
        }
    }

    public void call(String callId) {
        if(robot_id.equals("")){
            robot_id = "123456";
        }
        callID = callId;
        setFirebase();
        startCam();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VIDEO_CALL_SENT) {
            startCam();
        }
        switch(requestCode) {
            case REQUEST_ENABLE_BT:
                if(RESULT_OK != resultCode) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    finish();
                }
                break;
            default:
                break;
        }

        if (requestCode == SCANNER_CODE) {
            IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (scanningResult != null) {
                String scanContent = scanningResult.getContents();

                for (Medication med : todayMeds) {
                    if(scanContent.equals(med.getBarcode())) {
                        todayMeds.remove(med);
                        break;
                    }
                }
            }
            else{
                Toast.makeText(getApplicationContext(), "No scan data received!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void startCam() {
        client.start("Tele-Care Robot");
    }

    @Override
    public void onStatusChanged(final String newStatus) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), newStatus, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onLocalStream(MediaStream localStream) {
        localStream.videoTracks.get(0).addRenderer(new VideoRenderer(localRender));
        VideoRendererGui.update(localRender,
                LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING,
                LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING,
                scalingType);
    }

    @Override
    public void onAddRemoteStream(MediaStream remoteStream, int endPoint) {
        remoteStream.videoTracks.get(0).addRenderer(new VideoRenderer(remoteRender));
        VideoRendererGui.update(remoteRender,
                REMOTE_X, REMOTE_Y,
                REMOTE_WIDTH, REMOTE_HEIGHT, scalingType);
        VideoRendererGui.update(localRender,
                LOCAL_X_CONNECTED, LOCAL_Y_CONNECTED,
                LOCAL_WIDTH_CONNECTED, LOCAL_HEIGHT_CONNECTED,
                scalingType);
    }

    @Override
    public void onRemoveRemoteStream(int endPoint) {
        VideoRendererGui.update(localRender,
                LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING,
                LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING,
                scalingType);
    }

    public void setFirebase() {
        mFirebaseRef.child("users/robot_"+robot_id+"/rtsp_stream_url").setValue(mSocketAddress + "/" + callID);
    }

    public void ChangeID(View view) {
        String ID = ((EditText)findViewById(R.id.editText)).getText().toString();
        if (!ID.equals("")) {
            robot_id = ID;
            myTimerTask.changeID(ID);
            mFirebaseRef.child("users/robot_" + robot_id + "/bluetooth").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/char").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/hosp_ip").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/number1").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/number2").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/number3").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/robot_response").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/rtsp_stream_url").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/server_request").setValue("");
            mFirebaseRef.child("users/robot_" + robot_id + "/signal").setValue("");
        }
    }

    @Override
    public void onClick(View v){
        if(v.getId() == R.id.button5){
            IntentIntegrator scanIntegrator = new IntentIntegrator(this);
            scanIntegrator.initiateScan();
        }
    }
}

class BluetoothConnection extends Thread {
    private String robot_id;
    private InputStream inStream;
    private Firebase mFirebaseRef;
    private OutputStream outStream;
    private BluetoothSocket mmServerSocket;

    public BluetoothConnection(BluetoothDevice adapter, UUID uuid, Firebase FIREBASE, String robot) {
        BluetoothSocket tmp = null;
        inStream = null;
        outStream = null;
        robot_id = robot;
        mFirebaseRef = FIREBASE;
        try {
            tmp = adapter.createRfcommSocketToServiceRecord(uuid);
        } catch (IOException e) {}
        mmServerSocket = tmp;
    }

    public void run() {
        byte[] buffer = new byte[100];
        int bytes;
        while (true) {
            try {
                mmServerSocket.connect();
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
            if (mmServerSocket != null) {
                try {
                    inStream  = mmServerSocket.getInputStream();
                    outStream = mmServerSocket.getOutputStream();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                if (inStream != null) {
                    while (true) {
                        try {
                            bytes = inStream.read(buffer);
                            mFirebaseRef.child("users/robot_" + robot_id + "/bluetooth").setValue(bytes);
                        } catch (IOException e) {
                            break;
                        }
                    }
                }

                try {
                    mmServerSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public void write(byte[] bytes) {
        if (outStream != null) {
            try {
                outStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

class MyTimerTask extends TimerTask {
    private String robot_id;
    private Firebase mFirebaseRef;
    private WifiManager wifiManager;
    private LinkedList<Medication> medsList;
    private LinkedList<Medication> todayMeds;

    public MyTimerTask(Firebase FIREBASE, WifiManager WIFI, String ID,LinkedList<Medication> medsList,LinkedList<Medication> todayMeds) {
        robot_id = ID;
        mFirebaseRef = FIREBASE;
        wifiManager = WIFI;
        this.medsList = medsList;
        this.todayMeds = todayMeds;
    }

    public void changeID(String ID){
        robot_id = ID;
    }

    @Override
    public void run() {

        int numberOfLevels = 5;
        String st          = "";
        WifiInfo wifiInfo  = wifiManager.getConnectionInfo();
        int level          = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);

        if(level == 0){
            st = "0";
        }
        if(level == 1){
            st = "1";
        }
        if(level == 2){
            st = "2";
        }
        if(level == 3){
            st = "3";
        }
        if(level == 4){
            st = "4";
        }
        if(level == 5){
            st = "5";
        }
        mFirebaseRef.child("users/robot_" + robot_id + "/signal").setValue(st);

        Calendar rightNow = Calendar.getInstance();

        if(rightNow.get(Calendar.HOUR_OF_DAY) == 23){
            freshDay(medsList, todayMeds);
        }
    }

    public void freshDay(LinkedList<Medication> allMeds, LinkedList<Medication> todayMeds){
        todayMeds.clear();
        for (Medication med : allMeds) {
            todayMeds.add(med);
        }
    }
}