package project;

public class Medication {
	
	String name;
	int hour;
	int minute;
	String barcode;
	
	public Medication(String data){
		int spaceIndex = data.indexOf(" ");
		this.name = data.substring(0, spaceIndex);
		int colonIndex = data.indexOf(":");
		this.hour = Integer.parseInt(data.substring(spaceIndex + 1, colonIndex));
		spaceIndex = data.indexOf(" ", colonIndex);
		this.minute = Integer.parseInt(data.substring(colonIndex + 1, spaceIndex));
		this.barcode = data.substring(spaceIndex + 1, data.length());
	}

	public int getHour(){return hour;}
	public String getBarcode(){return barcode;}
	
}