(function(){
	var app = angular.module('projectRtc', [],
		function($locationProvider){$locationProvider.html5Mode(true);}
    );
	var client = new PeerManager();
	var mediaConfig = {
        audio:true,
        video: {
			mandatory: {},
			optional: []
        }
    };

    app.factory('camera', ['$rootScope', '$window', function($rootScope, $window){
    	var camera = {};
    	camera.preview = $window.document.getElementById('localVideo');

    	camera.start = function(){
			return requestUserMedia(mediaConfig)
			.then(function(stream){			
				attachMediaStream(camera.preview, stream);
				client.setLocalStream(stream);
				camera.stream = stream;
				$rootScope.$broadcast('cameraIsOn',true);
			})
			.catch(Error('Failed to get access to local media.'));
		};
    	camera.stop = function(){
    		return new Promise(function(resolve, reject){			
				try {
					camera.stream.stop();
					camera.preview.src = '';
					resolve();
				} catch(error) {
					reject(error);
				}
    		})
    		.then(function(result){
    			$rootScope.$broadcast('cameraIsOn',false);
    		});	
		};
		return camera;
    }]);

	app.controller('RemoteStreamsController', ['camera', '$location', '$http', '$rootScope', function(camera, $location, $http, $rootScope){
		var rtc = this;
		rtc.remoteStreams = [];
		function getStreamById(id) {
		    for(var i=0; i<rtc.remoteStreams.length;i++) {
		    	if (rtc.remoteStreams[i].id === id) {return rtc.remoteStreams[i];}
		    }
		}
		rtc.loadData = function () {
			// get list of streams from the server
			$http.get('/streams.json').success(function(data){
				// filter own stream
				var streams = data.filter(function(stream) {
			      	return stream.id != client.getId();
			    });
			    // get former state
			    for(var i=0; i<streams.length;i++) {
			    	var stream = getStreamById(streams[i].id);
			    	streams[i].isPlaying = (!!stream) ? stream.isPLaying : false;
			    }
			    // save new streams
			    rtc.remoteStreams = streams;
			});
		};

		rtc.view = function(stream){
			client.peerInit(stream.id);
			stream.isPlaying = !stream.isPlaying;
		};
		rtc.call = function(stream){
			/* If json isn't loaded yet, construct a new stream 
			 * This happens when you load <serverUrl>/<socketId> : 
			 * it calls socketId immediatly.
			**/
			if(!stream.id){
				stream = {id: stream, isPlaying: false};
				rtc.remoteStreams.push(stream);
			}
			if(camera.isOn){
				client.toggleLocalStream(stream.id);
				if(stream.isPlaying){
					client.peerRenegociate(stream.id);
				} else {
					client.peerInit(stream.id);
				}
				stream.isPlaying = !stream.isPlaying;
			} else {
				camera.start()
				.then(function(result) {
					client.toggleLocalStream(stream.id);
					if(stream.isPlaying){
						client.peerRenegociate(stream.id);
					} else {
						client.peerInit(stream.id);
					}
					stream.isPlaying = !stream.isPlaying;
				})
				.catch(function(err) {
					console.log(err);
				});
			}
		};
		
		//initial load
		rtc.loadData();
    	if($location.url() != '/'){
			console.log("Calling location " + $location.url().slice(1));
      		rtc.call($location.url().slice(1));
    	};
		

		$rootScope.$on('startVideoCall', function(event, url) {
			console.log("Starting video call to url = " + url + " id = " + url.split("/")[3]);
      		rtc.call(url.split("/")[3]);
		});
		
	}]);

	
	var rtcStreamLink = 0;
	
	app.controller('LocalStreamController',['camera', '$scope', '$window', function(camera, $scope, $window){
		var localStream = this;
		localStream.name = 'Controller';
		localStream.link = '';
		localStream.cameraIsOn = false;

		$scope.$on('cameraIsOn', function(event,data) {
    		$scope.$apply(function() {
		    	localStream.cameraIsOn = data;
		    });
		});

		localStream.toggleCam = function(){
			if(localStream.cameraIsOn){
				camera.stop()
				.then(function(result){
					client.send('leave');
	    			client.setLocalStream(null);
				})
				.catch(function(err) {
					console.log(err);
				});
			} else {
				camera.start()
				.then(function(result) {
					localStream.link = $window.location.host + '/' + client.getId();
					rtcStreamLink = localStream.link;
					$scope.$apply();
					client.send('readyToStream', { name: localStream.name });
				})
				.catch(function(err) {
					console.log(err);
				});
			}
		};
	}]);

	app.controller('RobotController', function($rootScope, $scope, $window) {
		var rc = this;
		rc.firebaseRef = {};
		rc.firebaseUsersRef = {};
		rc.users = [];
		rc.currentRobot = 0;
		rc.message_log = [];
		rc.signalStrength = 0;
		rc.fall = 0;
		rc.medications = [];
		rc.days = [];
		rc.last_seen=0;

		rc.connectToRobot = function(robot){
			rc.currentRobot = robot;
			
			rc.firebaseUsersRef.child(rc.currentRobot + "/robot_response").set("");
			rc.firebaseUsersRef.child(rc.currentRobot + "/rtsp_stream_url").set("");
			rc.firebaseUsersRef.child(rc.currentRobot + "/signal").set("");
			rc.firebaseUsersRef.child(rc.currentRobot + "/Fall").set("");  // *******
			rc.firebaseUsersRef.child(rc.currentRobot + "/Warning").set("");  // *******
			rc.firebaseUsersRef.child(rc.currentRobot + "/Light").set(0);  // *******
			rc.firebaseUsersRef.child(rc.currentRobot + "/Vid").set("https://www.google.co.il");  // *******
			rc.firebaseUsersRef.child(rc.currentRobot).on("child_changed", function(snapshot) {
				var field = snapshot.key();
				var robot_response = snapshot.val();
				if (field === "robot_response") {
					if (robot_response === "CONNECTION_OK") {
						rc.firebaseUsersRef.child(rc.currentRobot + "/robot_response").set("");
						rc.firebaseUsersRef.child(rc.currentRobot + "/server_request").set("");
						console.log("Successfully connected to id = " + rc.currentRobot);
					}
					if (robot_response === "MOVEMENT_OK") {
						rc.firebaseUsersRef.child(rc.currentRobot + "/robot_response").set("");
						rc.firebaseUsersRef.child(rc.currentRobot + "/server_request").set("");
						console.log("Successfully performed movement");
					}
					if (robot_response === "GET_DATA_OK") {
						rc.firebaseUsersRef.child(rc.currentRobot + "/robot_response").set("");
						console.log("Successfully received custom command");
					}
				}
				if (field === "rtsp_stream_url") {
					if (robot_response !== "") {
						console.log("Emitting event to start video call to url = " + robot_response);
						rc.firebaseUsersRef.child(rc.currentRobot + "/rtsp_stream_url").set("");
						$rootScope.$emit('startVideoCall', robot_response);
						
						//rtsp_stream_start(robot_response);
						//rc.firebaseUsersRef.child(rc.currentRobot + "/rtsp_stream_url").set("");
					}
				}
				if (field === "bluetooth") {
					rc.message_log[rc.message_log.length] = robot_response + "\n";
					console.log("Received bluetooth signal " + rc.message_log[rc.message_log.length - 1]);
					var list_to_string = "";
					for (var i in rc.message_log) {
						list_to_string += rc.message_log[rc.message_log.length - 1 - i];
					}
					$scope.robot_log = list_to_string;
					$scope.$apply();
				}
				if (field === "signal" || field === "Signal") {
					console.log("Received WiFi strength signal = " + robot_response);
					rc.signalStrength = robot_response;
					if (rc.signalStrength == 1 || rc.signalStrength == 2) {
						console.log("Bad signal, sending RETURN_TO_BASE command!");
						rc.moveRobot('RETURN_TO_BASE');
					}
					$scope.$apply();
				}
				//***************************
				if(field === "Fall"){

					if(robot_response==1) {
						console.log("im alerting!!");
						var sound1 = document.getElementById("audio2");
						sound1.play();
						alert("Fall has been Detected!");

					}
					rc.firebaseUsersRef.child(rc.currentRobot).child("Fall").set(0);
				}

				if(field === "Warning"){
					if(robot_response==1) {
						var sound2 = document.getElementById("audio3");
						sound2.play();
						alert("The Patient Did not take the medication!");

					}
					rc.firebaseUsersRef.child(rc.currentRobot).child("Warning").set(0);
				}







			});

			rc.firebaseUsersRef.child(rc.currentRobot).on("child_changed", function(snapshot){
				var field = snapshot.key();
				var medication = snapshot.val();
				if(field=="Medications") {
					rc.medications = [];
					for (var key in snapshot.val()) {
						rc.medications[rc.medications.length] = key;
					}
					$scope.medications = rc.medications;
					$scope.$apply();
				}

				});

			rc.firebaseUsersRef.child(rc.currentRobot).child("days").on("child_changed", function(snapshot){
				var field = snapshot.key();
				console.log(field);
				if(field=="day1"){
					rc.days[0]=snapshot.val();
				}
				if(field=="day2"){
					rc.days[1]=snapshot.val();
				}
				if(field=="day3"){
					rc.days[2]=snapshot.val();
				}
				if(field=="day4"){
					rc.days[3]=snapshot.val();
				}
				if(field=="day5"){
					rc.days[4]=snapshot.val();
				}
				if(field=="day6"){
					rc.days[5]=snapshot.val();
				}
				if(field=="day7"){
					rc.days[6]=snapshot.val();
				}
				$scope.days = rc.days;
				$scope.$apply();

			});

			rc.firebaseUsersRef.child(rc.currentRobot).on("child_changed", function(snapshot){
				var field = snapshot.key();
				console.log(field);
				if(field=="Motion"){
					rc.last_seen=snapshot.val();
				}


			});




			rc.firebaseUsersRef.child(rc.currentRobot + "/server_request").set("ISSUE_CONNECTION");
			rc.firebaseUsersRef.child(rc.currentRobot + "/host_ip").set($window.location.host);
			rc.databaseRetrieveMedications(rc.firebaseUsersRef.child(rc.currentRobot).child("Medications"));
			rc.databaseRetrieveDays();
			rc.databaseRetrieveLastSeen();
			console.log("Connected to: " + rc.currentRobot);


		}
		
		rc.databaseRetrieveUsers = function(users_ref){
			users_ref.once("value", function(snapshot) {

				rc.users.length = 0;
				for (var key in snapshot.val()) {
					console.log(key);
					rc.users[rc.users.length] = key;
					rc.firebaseUsersRef.child(key + "/server_request").set("");
				}
				
				$scope.robot_list = rc.users;
				$scope.$apply();

				
			});
		};

		// ********************************************************************
		rc.databaseRetrieveMedications = function(med_ref){

			med_ref.once("value", function(snapshot) {
				rc.medications = [];
				for (var key in snapshot.val()) {
					rc.medications[rc.medications.length] = key;

				}

				$scope.medications = rc.medications;
				$scope.$apply();


			});

		};


		rc.databaseRetrieveDays = function(){
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").child("day1").once("value",function(snapshot){
				rc.days[0]=snapshot.val();
			});
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").child("day2").once("value",function(snapshot){
				rc.days[1]=snapshot.val();
			});
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").child("day3").once("value",function(snapshot){
				rc.days[2]=snapshot.val();
			});
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").child("day4").once("value",function(snapshot){
				rc.days[3]=snapshot.val();
			});
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").child("day5").once("value",function(snapshot){
				rc.days[4]=snapshot.val();
			});
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").child("day6").once("value",function(snapshot){
				rc.days[5]=snapshot.val();
			});
			rc.firebaseUsersRef.child(rc.currentRobot).child("days").child("day7").once("value",function(snapshot){
				rc.days[6]=snapshot.val();
			});


		};

		rc.databaseRetrieveLastSeen=function(){
			rc.firebaseUsersRef.child(rc.currentRobot).child("Motion").once("value",function(snapshot){
				rc.last_seen=snapshot.val();
			});

		};
		
		rc.connectToFirebase = function(addr){
			console.log("Firebase addr = " + addr);
			rc.firebaseRef = new Firebase(addr);
			
			function authHandler(error, authData) {
				if (error) {
					console.log("Login Failed!", error);
				} else {
					console.log("Authenticated successfully with payload:", authData);
				}
			}

			rc.firebaseRef.authWithPassword({
			  email    : 'tele-care@gmail.com',
			  password : '12345678'
			}, authHandler);
			
			rc.firebaseRef.child("host_ip").set($window.location.host);
			rc.firebaseUsersRef = rc.firebaseRef.child("users");
			rc.databaseRetrieveUsers(rc.firebaseUsersRef);
		};
		
		rc.moveRobot = function(dir){
			rc.firebaseUsersRef.child(rc.currentRobot + "/server_request").set(dir);
		};
		// tofek added this ***************************************************************************
		rc.AddMedication = function(med,time,barcode){
			rc.firebaseUsersRef.child(rc.currentRobot).child("Medications").child(med).set(med+" "+time+" " +barcode);
			document.getElementById("100").value="";
			document.getElementById("time").value="";
			document.getElementById("barcode").value="";
			var sound6 = document.getElementById("audio6");
			sound6.play();

		};
		rc.RemoveMedication=function(med2){
			rc.firebaseUsersRef.child(rc.currentRobot).child("Medications").child(med2).remove();
			var sound7 = document.getElementById("audio7");
			sound7.play();


		};
		rc.boya=function(){
			window.alert("sunday : "+rc.days[0]+ "\n"+"monday : "+rc.days[1]
				+ "\n"+"tuesday : "+rc.days[2]+ "\n" +"wednesday : "+rc.days[3] + "\n" +
				"thursday : "+rc.days[4] +"\n" +"friday : "+rc.days[5]+"\n" +"saturday : "+rc.days[6]);

		};
		rc.lastseen=function(){
			window.alert("last seen: " + rc.last_seen);
		};

		rc.lamp=function(){
			var x=1000;
			if(rc.currentRobot!=0) {
				rc.firebaseUsersRef.child(rc.currentRobot).child("Light").once("value",function(snapshot){
					x=snapshot.val();
					if(x==0){
						var sound4 = document.getElementById("audio4");
						sound4.play();
						rc.firebaseUsersRef.child(rc.currentRobot).child("Light").set("1");

					}else{
						var sound5 = document.getElementById("audio5");
						sound5.play();
						rc.firebaseUsersRef.child(rc.currentRobot).child("Light").set("0");
					}
				});
			}

		};

		rc.addF = function(){
			rc.firebaseUsersRef.child(rc.currentRobot).child("Fall").set("1");
		};

		rc.vid = function(){
			console.log("rara1");
			rc.firebaseUsersRef.child(rc.currentRobot).child("Vid").once("value",function(snapshot){
				console.log("rara");
				window.open(snapshot.val(),'_blank');

			});


		};


		// end
		
		rc.sendCommand = function(chr, cmd1, cmd2, cmd3) {
			console.log("Sending command = " + chr + " " + cmd1 + " " + cmd2 + " " + cmd3);
			rc.firebaseUsersRef.child(rc.currentRobot + "/char").set(chr);
			rc.firebaseUsersRef.child(rc.currentRobot + "/number1").set(cmd1);
			rc.firebaseUsersRef.child(rc.currentRobot + "/number2").set(cmd2);
			rc.firebaseUsersRef.child(rc.currentRobot + "/number3").set(cmd3);
			rc.firebaseUsersRef.child(rc.currentRobot + "/server_request").set("GET_DATA");
		};
		
		
		rc.sendVideoRequest = function(){
			//rc.firebaseUsersRef.child(rc.currentRobot + "/rtsp_stream_url").set(rtcStreamLink);
			//console.log("Stream link = " + rtcStreamLink);
		}
		
		rc.stopVideoRequest = function(){
			//rc.firebaseUsersRef.child(rc.currentRobot + "/rtsp_stream_url").set("");
		}
	});
	
	
})();
